<?php
/**
 * Copyright (c) 2012-2015, AllWorldIT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.


 * Official Website:
 * https://gitlab.devlabs.linuxassist.net/awit-whmcs/whmcs-coza-epp/wikis/home

 * Lead developer:
 * Nigel Kukard <nkukard@lbsd.net>


 * ! ! P L E A S E   N O T E  ! !

 * * If you make changes to this file, please consider contributing
 *   anything useful back to the community. Don't be a sour prick.

 * * If you find this module useful please consider making a
 *   donation to support modules like this.


 * WHMCS hosting, theming, module development, payment gateway
 * integration, customizations and consulting all available from
 * http://allworldit.com

 * Buzybuy - cozaepp Disable Auto Renew
 * This script is a modification of the original cozaeppsync.php script from AllWorldIT combined with code from
 * Everything Web CC script.
 * This script will loop through all the coza domains in your WHMCS database.
 * Each domain's auto renew will be set to false on the domain.
 * This script can also be reversed and set each domain's auto renew to true by editing the flag value below.
 * http://www.buzybuy.com
 */

// This file brings in a few constants we need
require_once dirname(__FILE__) . '/../../../init.php';
// Setup include dir
$include_path = ROOTDIR . '/modules/registrars/cozaepp';
set_include_path($include_path . PATH_SEPARATOR . get_include_path());
// Include EPP stuff we need
require_once 'cozaepp.php';
// Additional functions we need
require_once ROOTDIR . '/includes/functions.php';
// Include registrar functions aswell
require_once ROOTDIR . '/includes/registrarfunctions.php';

// Grab module parameters
$params = getregistrarconfigoptions('cozaepp');

echo("COZA-EPP Domain Set Auto Renew To False Report\n");
echo("---------------------------------------------------\n");

// Request balance from registrar
try {
	$client = _cozaepp_Client();

	// Pull list of domains which are registered using this module
	$queryresult = mysql_query("
				SELECT
					domain
				FROM
					tbldomains
				WHERE registrar = 'cozaepp'"
			);
	while($data = mysql_fetch_array($queryresult)) {
		$domains[] = trim(strtolower($data['domain']));
	}

// Loop with each one
foreach ($domains as $domain) {
	sleep(1);

	// Get domain info
	$output = $client->request('
<epp xmlns="urn:ietf:params:xml:ns:epp-1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="urn:ietf:params:xml:ns:epp-1.0 epp-1.0.xsd">
	<command>
		<info>
			<domain:info xmlns:domain="urn:ietf:params:xml:ns:domain-1.0"
				xsi:schemaLocation="urn:ietf:params:xml:ns:domain-1.0 domain-1.0.xsd">
				<domain:name hosts="all">'.$domain.'</domain:name>
			</domain:info>
		</info>
		<extension>
			<cozadomain:info xmlns:cozadomain="http://co.za/epp/extensions/cozadomain-1-0"
				xsi:schemaLocation="http://co.za/epp/extensions/cozadomain-1-0 coza-domain-1.0.xsd">
			</cozadomain:info>
		</extension>
	</command>
</epp>
');

	$doc= new DOMDocument();
	$doc->loadXML($output);
	$coderes = $doc->getElementsByTagName('result')->item(0)->getAttribute('code');
	if ($coderes == '1000') {
		// Is autorenew enabled?
		if ($doc->getElementsByTagName('autorenew') &&
			$doc->getElementsByTagName('autorenew')->item(0) &&
			strtolower(trim($doc->getElementsByTagName('autorenew')->item(0)->nodeValue)) != "false"
		) {
			sleep(1);
			// Disable autorenew via update

			$output = $client->request('
<epp:epp xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:epp="urn:ietf:params:xml:ns:epp-1.0"
	xmlns:domain="urn:ietf:params:xml:ns:domain-1.0" xmlns:cozadomain="http://co.za/epp/extensions/cozadomain-1-0"
	xsi:schemaLocation="urn:ietf:params:xml:ns:epp-1.0 epp-1.0.xsd">
	<epp:command>
		<epp:update>
			<domain:update xsi:schemaLocation="urn:ietf:params:xml:ns:domain-1.0 domain-1.0.xsd">
				<domain:name>'.$domain.'</domain:name>
			</domain:update>
		</epp:update>
		<epp:extension>
			<cozadomain:update xsi:schemaLocation="http://co.za/epp/extensions/cozadomain-1-0 coza-domain-1.0.xsd">
				<cozadomain:chg>
					<cozadomain:autorenew>false</cozadomain:autorenew>
				</cozadomain:chg>
			</cozadomain:update>
		</epp:extension>
	</epp:command>
</epp:epp>
');

			$doc= new DOMDocument();
			$doc->loadXML($output);
			$coderes = $doc->getElementsByTagName('result')->item(0)->getAttribute('code');
			if($coderes == '1001') {
				if($doc->getElementsByTagName('msg')) {
					if($doc->getElementsByTagName('msg')->item(0)) {
						echo "- ".$domain." results: ".$doc->getElementsByTagName('msg')->item(0)->nodeValue."\n";
					} else {
						$status = "$domain response error\n";
						continue;
					}
				}
			} else {
				echo "Domain renew status update on $domain not successful: "._cozaepp_message($coderes)."\n";
				continue;
			}
		} else {
			echo "- ".$domain." already has auto-renew disabled.\n";
			continue;
		}
	} else {
		echo "Domain info on $domain not successful: "._cozaepp_message($coderes)."\n";
		continue;
	}

}

} catch (Exception $e) {
	echo("ERROR: ".$e->getMessage()."\n");
	exit;
}

